import request from '../utils/request'
import { rejected } from '../utils/rejectedAction'
import { ACTION_LOAD_PHOTOS, ACTION_SELECT_IMAGE, ACTION_UNSELECT_IMAGE } from '../constants/ActionTypes'
import { listImages } from './APIResponces'

// export const load = (collection = 'trainset2') => dispatch => request(`http://5.189.129.44:3000/collection/${collection}?format=json&options=dictTags,img_url,title`, { method: "GET" }).then(res => {
//
//   if(res.error) {
//     dispatch(rejected(ACTION_LOAD_PHOTOS));
//   } else {
//     let data = res.json();
//     dispatch({ type: ACTION_LOAD_PHOTOS, data})
//   }
// });

export const load = (collection = 'trainset2') => dispatch => dispatch({ type: ACTION_LOAD_PHOTOS, data: listImages });

export const select = img => ({ type: ACTION_SELECT_IMAGE, img });
export const unselect = img => ({ type: ACTION_UNSELECT_IMAGE, img });