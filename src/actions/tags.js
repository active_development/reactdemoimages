import request from '../utils/request'
import { rejected } from '../utils/rejectedAction'
import { ACTION_ADD_TAG, ACTION_REMOVE_TAG, ACTION_LOAD_TAGS, ACTION_ASSIGN_TAG } from '../constants/ActionTypes'
import { addTag, removeTag, listTags, assignTag } from './APIResponces'

export const load = () => dispatch => dispatch({ type: ACTION_LOAD_TAGS, data: listTags });
export const create = (name = 'Untitles') => dispatch => new Promise((resolve) => {
  let data = addTag(name);
  dispatch({ type: ACTION_ADD_TAG, data });

  resolve(data);
});
export const remove = _id => dispatch => dispatch({ type: ACTION_REMOVE_TAG, data: removeTag(_id) });
export const assign = (tab, imgs) => dispatch => new Promise((resolve) => {
  let data = assignTag(tab, imgs);
  dispatch({ type: ACTION_ASSIGN_TAG, data });

  resolve(data);
});

export const createAndAssign = (imgs = []) => dispatch => name => create(name)(dispatch).then(tag => assign(tag, imgs)(dispatch));