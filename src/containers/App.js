import React, { Component, PropTypes } from 'react';
import { Grid } from 'react-bootstrap'

import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';

class App extends Component {
  render() {
    return (
      <div className="main-app-container">
        <Sidebar />
        <div className="workspace-wrapper">
          <div>{ this.props.children }</div>
          <Footer />
        </div>
      </div>
    );
  }
}

App.propTypes = {

};
export default App
