import { concatEventReducers } from '../utils/concatEventReducers'
import { ACTION_ADD_TAG, ACTION_REMOVE_TAG, ACTION_LOAD_TAGS } from '../constants/ActionTypes'

export const list = concatEventReducers({
  [ACTION_LOAD_TAGS]: (state = {}, { data }) => ({ ...state, ...data.reduce((acc, el) => ({ ...acc, [el._id]: el }), {}) }),
  [ACTION_ADD_TAG]: (state = {}, { data }) => ({ ...state, [data._id]: data }),
  [ACTION_REMOVE_TAG]: (state = {}, { data }) => {
    delete state[data._id];
    return { ...state };
  },
  default : (state = {}) => state || {}
});