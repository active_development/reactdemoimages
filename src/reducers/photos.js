import { concatEventReducers } from '../utils/concatEventReducers'
import { ACTION_LOAD_PHOTOS, ACTION_SELECT_IMAGE, ACTION_UNSELECT_IMAGE, ACTION_ASSIGN_TAG } from '../constants/ActionTypes'

export const list = concatEventReducers({
  [ACTION_LOAD_PHOTOS]: (state = {}, { data }) => data.reduce((acc, el) => ({...acc, [el._id]: el}), {}),
  [ACTION_ASSIGN_TAG]: (state = {}, { data: { tag, images } }) => ({...state, ...images.reduce((acc, id) => {
    let img = state[id];
    let { tags = {} } = img;

    return {...acc, [id]: {...img, tags: {...tags, [tag]: true}}}
  }, {})}),
  default : (state = {}) => state || {}
});

export const selected = concatEventReducers({
  [ACTION_SELECT_IMAGE]: (state = {}, { img }) => ({ ...state, [img._id]: true }),
  [ACTION_UNSELECT_IMAGE]: (state = {}, { img }) => ({ ...state, [img._id]: false }),
  default: state => state || {}
});