import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import { bindActionCreators } from 'redux'
import { create, remove } from '../../actions/tags'
import { connect } from 'react-redux'
import { ControlLabel, FormGroup, FormControl, Button, Row, Col } from 'react-bootstrap'

class Form extends Component {
  submit() {
    return e => {
      e.preventDefault();
      // let data = new FormData(findDOMNode(this.refs.form));
      let name = findDOMNode(this.refs.name).value;
      this.props.action(name);

      findDOMNode(this.refs.name).value = ""
    }
  }

  render() {
    return (
      <form ref="form" onSubmit={this.submit()}>
        <FormGroup>
          <FormControl ref="name" name="name" placeholder="Tag name..."/>
        </FormGroup>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ remove }, dispatch);

export default connect(undefined, mapDispatchToProps)(Form)