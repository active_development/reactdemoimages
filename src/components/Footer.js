import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <footer className="text-center">
        <strong id="footer-text-wrap">Igor Olshevsky</strong>
      </footer>
    );
  }
}
