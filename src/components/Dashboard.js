import _ from 'lodash'
import React, { Component } from 'react'
import { Grid, Col, Row } from 'react-bootstrap'
import ImgContainer from './photos/ImgContainer'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { defer } from '../utils/defer'
import { load as loadImages, select, unselect } from '../actions/photos'
import { load as loadTags } from '../actions/tags'

@defer(({ props: { loadImages } }) => loadImages())
@defer(({ props: { loadTags } }) => loadTags())
class Dashboard extends Component {
  render() {
    let { list, selected } = this.props;
    let chunks = _.chunk(list, 4);
    return (
      <div>
        <h1 className="text-center">{ list.length } image(s)</h1>
        <Grid fluid>
          { chunks.map((images, rowId) => (
            <Row key={rowId} className="img-row">
              { images.map((img, imgId) => (
                <Col key={imgId} md={3}>
                  <ImgContainer active={selected.includes(img._id)} onClick={!selected.includes(img._id) ? this.props.select : this.props.unselect} img={img}/>
                </Col>
              )) }
            </Row>
          )) }
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ photos: { list, selected } }) => ({
  list: Object.keys(list).map(el => list[el]),
  selected: Object.keys(selected).filter(id => selected[id])
});
const mapDispatchToProps = dispatch => bindActionCreators({ loadImages, loadTags, select, unselect }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)