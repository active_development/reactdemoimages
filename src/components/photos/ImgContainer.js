import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Button } from 'react-bootstrap'

class ImgContainer extends Component {
  handle() {
    return e => {
      let { img, onClick } = this.props;
      onClick(img)
    }
  }

  render() {
    let { img, onClick, active, tagsList } = this.props;
    console.log(tagsList)
    let { tags = {} } = img;
    return (
      <div onClick={this.handle()} className={` img-thumb ${active && 'selected'} `} style={{ padding: "15px" }}>
        <img style={{ width: " 100% " }} src={img.img_url} />
        { Object.keys(tags).length ? <p>{ Object.keys(tags).map(tagId => <Button bsSize="xsmall" bsStyle="success">{ tagsList[tagId].name }</Button>) }</p> : null}
      </div>
    );
  }
}

ImgContainer.propTypes = {
  img: PropTypes.object,
  onClick: PropTypes.func
};

const mapStateToProps = ({ tags: { list } }) => ({tagsList: list});

export default connect(mapStateToProps)(ImgContainer)