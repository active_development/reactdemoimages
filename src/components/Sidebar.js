import React, { Component, PropTypes } from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { remove, assign, createAndAssign, create} from '../actions/tags'
import Form from './tags/Form'
import { connect } from 'react-redux'

class Sidebar extends Component {
  handleSubmit() {

  }

  render() {
    let { list, selected } = this.props;
    // <Button onClick={e => this.props.remove(tag._id)} bsStyle="danger" bsSize="xsmall">{`$times;`}</Button>
    return (
      <div className={`main-app-nav sidebar`}>
        <h2 className="text-center">Tags</h2>
        <Form action={selected.length ? this.props.createAndAssign(selected) : this.props.create} />
        <div>
          { list.map((tag, index) => (
            <ButtonGroup key={index} bsStyle="success" bsSize="xsmall">
              <Button onClick={e => selected.length && this.props.assign(tag, selected)} bsStyle="success" bsSize="xsmall">{ tag.name }</Button>
            </ButtonGroup>
          )) }
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {

};

const mapStateToProps = ({ tags: { list }, photos: { selected } }) => ({ list: Object.keys(list).map(id => list[id]), selected: Object.keys(selected).filter(id => selected[id]) });
const mapDispatchToProps = dispatch => bindActionCreators({ remove, create, assign, createAndAssign }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)