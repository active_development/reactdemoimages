import React from 'react'
import { connect } from 'react-redux'

export function defer(action) {
  return Component => {
    class DeferComponent extends Component {
      componentDidMount() {
        super.componentDidMount && super.componentDidMount()
        action({params: this.props.params || {}, props: this.props, query: this.props.location});
      }
    }

    return DeferComponent
  }
}